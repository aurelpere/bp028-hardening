# bp028-hardening

## ansible bp028 role

Ansible roles to adapt anssi bp028 guide v.1.2 and v2.0 here https://www.ssi.gouv.fr/guide/recommandations-de-securite-relatives-a-un-systeme-gnulinux/


## contributions and needed improvements:

adapt roles to debian and rhel (almalinux/fedora) with automatic variables

fine grained become statement

check iptables and docker roles. Verify default forward iptables policy for containers with an exposed port to avoid exposing database (in particular with one container connected to 2 networks : 1 for web and 1 for internal use)

implementation of missing bp028 roles: sudo config, selinux config, pam (passwords) config, log config, fix permissions role wreaking passwords (?), elaborate permissions role

implementation of a working compile-kernel role and corresponding sysctl kernel parameters (5.2.1 in anssi bp028 guide v.2)

implementation of missing kernel hardening options : https://madaidans-insecurities.github.io/guides/linux-hardening.html

implementation of archwiki security (hardened malloc, improve suidsgid role, etc.) https://wiki.archlinux.org/title/security

implementation of openclassroom security audit rules https://openclassrooms.com/fr/courses/2035746-auditez-la-securite-dun-systeme-dexploitation

implementation of improved security regarding services. See example of lynis audit regarding default containerd service:

```[ ~]$ /usr/bin/systemd-analyze security containerd<br>
  NAME                                                        DESCRIPTION   <br>                                                         >
✗ RootDirectory=/RootImage=                                   Service runs within the host's root directory <br>                         >
  SupplementaryGroups=                                        Service runs as root, option does not matter<br>                           >
  RemoveIPC=                                                  Service runs as root, option does not apply<br>                            >
✗ User=/DynamicUser=                                          Service runs as root user<br>                                              >
✗ CapabilityBoundingSet=~CAP_SYS_TIME                         Service processes may change the system clock<br>                          >
✗ NoNewPrivileges=                                            Service processes may acquire new privileges<br>                           >
✓ AmbientCapabilities=                                        Service process does not receive ambient capabilities<br>                  >
✗ PrivateDevices=                                             Service potentially has access to hardware devices<br>                     >
✗ ProtectClock=                                               Service may write to the hardware clock or system clock<br>                >
✗ CapabilityBoundingSet=~CAP_SYS_PACCT                        Service may use acct()<br>                                                 >
✗ CapabilityBoundingSet=~CAP_KILL                             Service may send UNIX signals to arbitrary processes<br>                   >
✗ ProtectKernelLogs=                                          Service may read from or write to the kernel log ring buffer<br>           >
✗ CapabilityBoundingSet=~CAP_WAKE_ALARM                       Service may program timers that wake up the system<br>                     >
✗ CapabilityBoundingSet=~CAP_(DAC_*|FOWNER|IPC_OWNER)         Service may override UNIX file/IPC permission checks<br>                   >
✗ ProtectControlGroups=                                       Service may modify the control group file system<br>                       >
✗ CapabilityBoundingSet=~CAP_LINUX_IMMUTABLE                  Service may mark files immutable<br>                                       >
✗ CapabilityBoundingSet=~CAP_IPC_LOCK                         Service may lock memory into RAM<br>                                       >
✗ ProtectKernelModules=                                       Service may load or read kernel modules<br>                                >
✗ CapabilityBoundingSet=~CAP_SYS_MODULE                       Service may load kernel modules<br>                                        >
✗ CapabilityBoundingSet=~CAP_SYS_TTY_CONFIG                   Service may issue vhangup()<br>                                            >
✗ CapabilityBoundingSet=~CAP_SYS_BOOT                         Service may issue reboot()<br>                                             >
✗ CapabilityBoundingSet=~CAP_SYS_CHROOT                       Service may issue chroot()<br>                                             >
✗ PrivateMounts=                                              Service may install system mounts<br>                                      >
✗ SystemCallArchitectures=                                    Service may execute system calls with all ABIs<br>                         >
✗ CapabilityBoundingSet=~CAP_BLOCK_SUSPEND                    Service may establish wake locks<br>                                       >
✗ MemoryDenyWriteExecute=                                     Service may create writable executable memory mappings<br>                 >
✗ RestrictNamespaces=~user                                    Service may create user namespaces<br>                                     >
✗ RestrictNamespaces=~pid                                     Service may create process namespaces<br>                                  >
✗ RestrictNamespaces=~net                                     Service may create network namespaces<br>                                  >
✗ RestrictNamespaces=~uts                                     Service may create hostname namespaces<br>                                 >
✗ RestrictNamespaces=~mnt                                     Service may create file system namespaces<br>                              >
✗ CapabilityBoundingSet=~CAP_LEASE                            Service may create file leases<br>                                         >
✗ CapabilityBoundingSet=~CAP_MKNOD                            Service may create device nodes<br>                                        >
✗ RestrictNamespaces=~cgroup                                  Service may create cgroup namespaces<br>                                   >
✗ RestrictSUIDSGID=                                           Service may create SUID/SGID files<br>                                     >
✗ RestrictNamespaces=~ipc                                     Service may create IPC namespaces<br>                                      >
✗ ProtectHostname=                                            Service may change system host/domainname<br>                              >
✗ CapabilityBoundingSet=~CAP_(CHOWN|FSETID|SETFCAP)           Service may change file ownership/access mode/capabilities unrestricted><br>
✗ CapabilityBoundingSet=~CAP_SET(UID|GID|PCAP)                Service may change UID/GID identities/capabilities<br>                     >
✗ LockPersonality=                                            Service may change ABI personality<br>                                     >
✗ ProtectKernelTunables=                                      Service may alter kernel tunables<br>                                      >
✗ RestrictAddressFamilies=~AF_PACKET                          Service may allocate packet sockets<br>                                    >
✗ RestrictAddressFamilies=~AF_NETLINK                         Service may allocate netlink sockets<br>                                   >
✗ RestrictAddressFamilies=~AF_UNIX                            Service may allocate local sockets<br>                                     >
✗ RestrictAddressFamilies=~…                                  Service may allocate exotic sockets<br>                                    >
✗ RestrictAddressFamilies=~AF_(INET|INET6)                    Service may allocate Internet sockets<br>                                  >
✗ CapabilityBoundingSet=~CAP_MAC_*                            Service may adjust SMACK MAC<br>                                           >
✗ RestrictRealtime=                                           Service may acquire realtime scheduling<br>                                >
✗ Delegate=                                                   Service maintains its own delegated control group subtree<br>              >
✗ CapabilityBoundingSet=~CAP_SYS_RAWIO                        Service has raw I/O access<br>                                             >
✗ CapabilityBoundingSet=~CAP_SYS_PTRACE                       Service has ptrace() debugging abilities<br>                               >
✗ CapabilityBoundingSet=~CAP_SYS_(NICE|RESOURCE)              Service has privileges to change resource use parameters<br>               >
✗ DeviceAllow=                                                Service has no device ACL<br>                                              >
✗ CapabilityBoundingSet=~CAP_NET_ADMIN                        Service has network configuration privileges<br>                           >
✗ ProtectSystem=                                              Service has full access to the OS file hierarchy<br>                       >
✗ ProtectProc=                                                Service has full access to process tree (/proc hidepid=)<br>               >
✗ ProcSubset=                                                 Service has full access to non-process /proc files (/proc subset=)<br>     >
✗ ProtectHome=                                                Service has full access to home directories<br>                            >
✗ CapabilityBoundingSet=~CAP_NET_(BIND_SERVICE|BROADCAST|RAW) Service has elevated networking privileges<br>                             >
✗ CapabilityBoundingSet=~CAP_AUDIT_*                          Service has audit subsystem access<br>                                     >
✗ CapabilityBoundingSet=~CAP_SYS_ADMIN                        Service has administrator privileges<br>                                   >
✗ PrivateNetwork=                                             Service has access to the host's network<br>                               >
✗ PrivateUsers=                                               Service has access to other users<br>                                      >
✗ PrivateTmp=                                                 Service has access to other software's temporary files<br>                 >
✗ CapabilityBoundingSet=~CAP_SYSLOG                           Service has access to kernel logging<br>                                   >
✓ KeyringMode=                                                Service doesn't share key material with other services<br>                 >
✗ SystemCallFilter=~@clock                                    Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@cpu-emulation                            Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@debug                                    Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@module                                   Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@mount                                    Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@obsolete                                 Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@privileged                               Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@raw-io                                   Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@reboot                                   Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@resources                                Service does not filter system calls<br>                                   >
✗ SystemCallFilter=~@swap                                     Service does not filter system calls<br>                                   >
✗ IPAddressDeny=                                              Service does not define an IP address allow list<br>                       >
✓ NotifyAccess=                                               Service child processes cannot alter service state<br>                     >
✗ UMask=                                                      Files created by service are world-readable by default<br>                 >

→ Overall exposure level for containerd.service: 9.6 UNSAFE 😨<br>```

