#!/bin/bash
#sources : wiki ubuntu iptables, archwiki security firewall, ziegler linux firewalls
#verifier si output accept risqué?
set -ex
IPT=iptables # Location of $IPT on your system
INTERNET=”_interface_” # Internet-connected interface
LOOPBACK_INTERFACE=”lo” # however your system names it
IPADDR=10.10.1.22 # your IP address
PUBLIC_IP_ADRESS="$(curl ifconfig.me)"
#MY_ISP=”my.isp.address.range” # ISP server & NOC address range
#SUBNET_BASE=”my.subnet.network” # Your subnet’s network address
#SUBNET_BROADCAST=”my.subnet.bcast” # Your subnet’s broadcast address
LOOPBACK=127.0.0.0/8 # reserved loopback address range
CLASS_A=10.0.0.0/8 # class A private networks
CLASS_B=172.16.0.0/12 # class B private networks
CLASS_C=192.168.0.0/16 # class C private networks
CLASS_D_MULTICAST=224.0.0.0/4 # class D multicast addresses
CLASS_E_RESERVED_NET=240.0.0.0/5 # class E reserved addresses
CLASS_DOCKER=172.0.0.0/8
BROADCAST_SRC=”0.0.0.0” # broadcast source address
BROADCAST_DEST=”255.255.255.255” # broadcast destination address
PRIVPORTS=”0:1023” # well-known, privileged port range
UNPRIVPORTS=”1024:65535” # unprivileged port range


$IPT -F
$IPT -X
$IPT -t nat -F
$IPT -t nat -X
$IPT -t mangle -F
$IPT -t mangle -X

$IPT -N TCP
$IPT -N DOCKER
$IPT -N UDP
$IPT -N DOCKER-USER

$IPT --policy INPUT ACCEPT #dropped at the end of script
$IPT --policy OUTPUT ACCEPT
$IPT --policy FORWARD ACCEPT #dropped at the end of script

$IPT -A INPUT -i lo -j ACCEPT
$IPT -A OUTPUT -o lo -j ACCEPT

$IPT -t mangle --policy PREROUTING ACCEPT
$IPT -t mangle --policy OUTPUT ACCEPT


############################DROP des scan,invalid, multicast, broadcast############################
## On drop le Multicast.
$IPT -A INPUT -m pkttype --pkt-type multicast -j DROP

## Dropper silencieusement tous les paquets broadcastés.
$IPT -A INPUT -m pkttype --pkt-type broadcast -j DROP

## On drop les scans XMAS et NULL.
$IPT -A INPUT -m conntrack --ctstate INVALID -p tcp --tcp-flags FIN,URG,PSH FIN,URG,PSH -j DROP
$IPT -A INPUT -m conntrack --ctstate INVALID -p tcp --tcp-flags ALL ALL -j DROP
$IPT -A INPUT -m conntrack --ctstate INVALID -p tcp --tcp-flags ALL NONE -j DROP
$IPT -A INPUT -m conntrack --ctstate INVALID -p tcp --tcp-flags SYN,RST SYN,RST -j DROP


# All of the bits are cleared
$IPT -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
# SYN and FIN are both set
$IPT -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
# SYN and RST are both set
$IPT -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
# FIN and RST are both set
$IPT -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
# FIN is the only bit set, without the expected accompanying ACK
$IPT -A INPUT -p tcp --tcp-flags ACK,FIN FIN -j DROP
# PSH is the only bit set, without the expected accompanying ACK
$IPT -A INPUT -p tcp --tcp-flags ACK,PSH PSH -j DROP
# URG is the only bit set, without the expected accompanying ACK
$IPT -A INPUT -p tcp --tcp-flags ACK,URG URG -j DROP

# drop invalid packets in input
$IPT -A INPUT -m conntrack --ctstate INVALID -j DROP

############################DROP ping############################
#accept first icmp packet rule
#$IPT -A INPUT -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
$IPT -A INPUT -p icmp --icmp-type echo-request -j DROP

############################ACCEPT des related,established############################
# accept input related established
$IPT -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

############################DROP/REJECT paquets from internet (spoof) ############################
# Refuse spoofed packets pretending to be from
# the external interface’s IP address
$IPT -A INPUT -i $INTERNET -s $IPADDR -j DROP
# Server rule Refuse packets claiming to be from public ip adress
$IPT -A INPUT -i $INTERNET -s $PUBLIC_IP_ADRESS -j DROP
# Refuse packets claiming to be from a Class A private network
$IPT -A INPUT -i $INTERNET -s $CLASS_A -j DROP
# Refuse packets claiming to be from a Class B private network
$IPT -A INPUT -i $INTERNET -s $CLASS_B -j DROP
# Refuse packets claiming to be from a Class C private network
$IPT -A INPUT -i $INTERNET -s $CLASS_C -j DROP
# Refuse packets claiming to be from the loopback interface
$IPT -A INPUT -i $INTERNET -s $LOOPBACK -j DROP


############################Jump TCP/UDP pour les NEW avec BUFFER 60s############################
# jump to UDP and TCP chain
$IPT -A INPUT -p udp -m conntrack --ctstate NEW -j UDP
$IPT -A INPUT -p tcp -m conntrack --syn --ctstate NEW -j TCP
# reject if port not open for udp, tcp and other protocols
$IPT -A INPUT -p udp -m recent --set --rsource --name UDP-PORTSCAN -j REJECT --reject-with icmp-port-unreachable
$IPT -A INPUT -p tcp -m recent --set --rsource --name TCP-PORTSCAN -j REJECT --reject-with tcp-reset
$IPT -A INPUT -j REJECT --reject-with icmp-proto-unreachable

############################Règles sur les ports TCP/UDP ouverts ajoutés par ansible############################
#voir si utile de mettre cette ligne décommentée...
#$IPT -I TCP -p tcp -m recent --update --rsource --seconds 60 --name TCP-PORTSCAN -j REJECT --reject-with tcp-reset 
# server open ports:
##### TCP #####
#$IPT -A TCP -p tcp --dport 80 -j ACCEPT
#$IPT -A TCP -p tcp --dport 443 -j ACCEPT
#$IPT -A TCP -p tcp --dport 22 -j ACCEPT

$IPT -I TCP -p tcp -m recent --update --rsource --seconds 60 --name TCP-PORTSCAN -j REJECT --reject-with tcp-reset

#### UDP ####
$IPT -I UDP -p udp -m recent --update --rsource --seconds 60 --name UDP-PORTSCAN -j REJECT --reject-with icmp-port-unreachable


####DOCKER-USER#####
############################DROP/REJECT paquets from internet (spoof) ############################
$IPT -A DOCKER-USER -i $INTERNET -s $CLASS_DOCKER -j DROP

############################DROP des scan,invalid, multicast, broadcast############################
## On drop le Multicast.
$IPT -A DOCKER-USER -m pkttype --pkt-type multicast -j DROP

## Dropper silencieusement tous les paquets broadcastés.
$IPT -A DOCKER-USER -m pkttype --pkt-type broadcast -j DROP

## On drop les scans XMAS et NULL.
$IPT -A DOCKER-USER -m conntrack --ctstate INVALID -p tcp --tcp-flags FIN,URG,PSH FIN,URG,PSH -j DROP
$IPT -A DOCKER-USER -m conntrack --ctstate INVALID -p tcp --tcp-flags ALL ALL -j DROP
$IPT -A DOCKER-USER -m conntrack --ctstate INVALID -p tcp --tcp-flags ALL NONE -j DROP
$IPT -A DOCKER-USER -m conntrack --ctstate INVALID -p tcp --tcp-flags SYN,RST SYN,RST -j DROP


# All of the bits are cleared
$IPT -A DOCKER-USER -p tcp --tcp-flags ALL NONE -j DROP
# SYN and FIN are both set
$IPT -A DOCKER-USER -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
# SYN and RST are both set
$IPT -A DOCKER-USER -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
# FIN and RST are both set
$IPT -A DOCKER-USER -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
# FIN is the only bit set, without the expected accompanying ACK
$IPT -A DOCKER-USER -p tcp --tcp-flags ACK,FIN FIN -j DROP
# PSH is the only bit set, without the expected accompanying ACK
$IPT -A DOCKER-USER -p tcp --tcp-flags ACK,PSH PSH -j DROP
# URG is the only bit set, without the expected accompanying ACK
$IPT -A DOCKER-USER -p tcp --tcp-flags ACK,URG URG -j DROP

# drop invalid packets in input, accept first icmp packet
$IPT -A DOCKER-USER -m conntrack --ctstate INVALID -j DROP

############################ACCEPT des related,established############################
# accept input related established
$IPT -A DOCKER-USER -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

############################DROP/REJECT paquets from internet (spoof) ############################
# Refuse spoofed packets pretending to be from
# the external interface’s IP address
$IPT -A DOCKER-USER -i $INTERNET -s $IPADDR -j DROP
# Refuse packets claiming to be from a Class A private network
$IPT -A DOCKER-USER -i $INTERNET -s $CLASS_A -j DROP
# Refuse packets claiming to be from a Class B private network
$IPT -A DOCKER-USER -i $INTERNET -s $CLASS_B -j DROP
# Refuse packets claiming to be from a Class C private network
$IPT -A DOCKER-USER -i $INTERNET -s $CLASS_C -j DROP
# Refuse packets claiming to be from the loopback interface
$IPT -A DOCKER-USER -i $INTERNET -s $LOOPBACK -j DROP

############################DROP ping############################
#accept first icmp packet rule
#$IPT -A DOCKER-USER -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
$IPT -A DOCKER-USER -p icmp --icmp-type echo-request -j DROP

##########################jump to UDPDOCKER and TCPDOCKER chai pour les from internet##############################
# jump to UDPDOCKER and TCPDOCKER chain
$IPT -A DOCKER-USER -i $INTERNET -s $CLASS_DOCKER -j DROP

$IPT -A DOCKER-USER -i $INTERNET -p udp -m conntrack --ctstate NEW -j DOCKER
$IPT -A DOCKER-USER -i $INTERNET -p tcp --syn -m conntrack --ctstate NEW -j DOCKER
# reject if port not open for udp, tcp and other protocols
$IPT -A DOCKER-USER -i $INTERNET -p udp -m recent --set --rsource --name UDP-PORTSCAN -j REJECT --reject-with icmp-port-unreachable
$IPT -A DOCKER-USER -i $INTERNET -p tcp -m recent --set --rsource --name TCP-PORTSCAN -j REJECT --reject-with tcp-reset
$IPT -A DOCKER-USER -i $INTERNET -j REJECT --reject-with icmp-proto-unreachable

$IPT --policy INPUT DROP
$IPT --policy OUTPUT ACCEPT
$IPT --policy FORWARD DROP
