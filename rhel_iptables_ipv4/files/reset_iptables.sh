#/bin/bash
set -ex

# Flush all existing rules, chains, and tables
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X

# create table
# Not necessary in iptables

# set default policy to drop
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT

