---
# defaults file for kernel

kernel_version: "6.1.7"

kernel_build_location: /home/{{ ansible_user }}

kernel_parameters:
  - name: CONFIG_SMP
    value: y

######################## gestion memoire bp028 ##############################
#
## Cette option a remplacé CONFIG_DEBUG_RODATA dans le noyau ( >=4.11)
## qui était une dépendance de CONFIG_DEBUG_KERNEL
#  - name: CONFIG_STRICT_KERNEL_RWX
#    value: y
## CONFIG_ARCH_OPTIONAL_KERNEL_RWX et CONFIG_ARCH_HAS_STRICT_KERNEL_RWX sont
## des dépendances de CONFIG_STRICT_KERNEL_RWX
#  - name: CONFIG_ARCH_OPTIONAL_KERNEL_RWX
#    value: y
#  - name: CONFIG_ARCH_HAS_STRICT_KERNEL_RWX
#    value: y
## Vérifie et rapporte les permissions de mapping mémoire dangereuses , par
## exemple , lorsque les pages noyau sont en écriture et sont exécutables.
## Cette option n'est pas disponible sur l'ensemble des architectures
## matérielles (x86 >=4.4, arm64 >=4.10, etc.).
#  - name: CONFIG_DEBUG_WX
#    value: y
## Désactive le système de fichiers utilisé uniquement dans le cadre de
## débogage. Protéger ce système de fichiers nécessite un travail
## supplémentaire.
#  - name: CONFIG_DEBUG_FS
#    value: n
## A partir de la version 4.18 du noyau , ces options ajoutent
## -fstack -protector -strong à la compilation afin de renforcer le stack canary ,
## il est nécessaire d'avoir une version de GCC au moins égale à 4.9.
## Avant la version 4.18 du noyau linux , il faut utiliser les options
## CONFIG_CC_STACKPROTECTOR et CONFIG_CC_STACKPROTECTOR_STRONG
#  - name: CONFIG_STACKPROTECTOR
#    value: y
#  - name: CONFIG_STACKPROTECTOR_STRONG
#    value: y
## Détecte la corruption de la pile pendant l'appel à l'ordonnanceur
#  - name: CONFIG_SCHED_STACK_END_CHECK
#    value: y
## Impose une vérification des limites du mapping mémoire du processus au
## moment des copies de données.
#  - name: CONFIG_HARDENED_USERCOPY
#    value: y
## Ajout de pages de garde entre les piles noyau. Cela protège contre les effets
## de bord des débordements de pile (cette option n'est pas supportée sur toutes
## les architectures).
#  - name: CONFIG_VMAP_STACK
#    value: y
## Vérifications exhaustives sur les compteurs de référence du noyau ( <=5.4)
#  - name: CONFIG_REFCOUNT_FULL
#    value: y
## Vérifie les actions de recopie mémoire qui pourraient provoquer une corruption
## de structure dans les fonctions noyau str *() et mem *(). Cette vérification est
## effectuée à la compilation et à l'exécution.
#  - name: CONFIG_FORTIFY_SOURCE
#    value: y
## Empèche les utilisateurs non privilégiés de récupérer des adresses noyau
## avec dmesg (8)
#  - name: CONFIG_SECURITY_DMESG_RESTRICT
#    value: y
## Active les retpoline qui sont nécessaires pour se protéger de Spectre v2
## GCC >= 7.3.0 est requis.
#  - name: CONFIG_RETPOLINE
#    value: y
## Désactive la table vsyscall. Elle n'est plus requise par la libc et est une
## source potentielle de ROP gadgets.
#  - name: CONFIG_LEGACY_VSYSCALL_NONE
#    value: y
#  - name: CONFIG_LEGACY_VSYSCALL_EMULATE
#    value: n
#  - name: CONFIG_LEGACY_VSYSCALL_XONLY
#    value: n
#  - name: CONFIG_X86_VSYSCALL_EMULATION
#    value: n
#
########################## structures de données bp028 ##############################
#
## Vérifie les structures de gestion des autorisations.
#  - name: CONFIG_DEBUG_CREDENTIALS
#    value: y
## Vérifie les structures de notification.
#  - name: CONFIG_DEBUG_NOTIFIERS
#    value: y
## Vérifie les listes noyau.
#  - name: CONFIG_DEBUG_LIST
#    value: y
## Vérifie les tables de Scatter -Gather du noyau.
#  - name: CONFIG_DEBUG_SG
#    value: y
## Génère un appel à BUG() en cas de détection de corruption.
#  - name: CONFIG_BUG_ON_DATA_CORRUPTION
#    value: y
#
############################### allocateur memoire bp028 ##############################
## Positionne les informations de chaînage des blocs libres de l'allocateur de
## manière aléatoire.
#  - name: CONFIG_SLAB_FREELIST_RANDOM
#    value: y
## CONFI_SLAB est une dépendance de CONFIG_SLAB_FREELIST_RANDOM
#  - name: CONFIG_SLUB
#    value: y
## Protège les métadonnées de l'allocateur en intégrité.
#  - name: CONFIG_SLAB_FREELIST_HARDENED
#    value: y
## A partir de la version 4.13 du noyau , cette option désactive le merge des
## caches SLAB
#  - name: CONFIG_SLAB_MERGE_DEFAULT
#    value: n
## Active la vérification des structures de l'allocateur mémoire et réinitialise
## à zéro les zones allouées à leur libération (nécessite l'activation de
## l'option de configuration de la mémoire page_poison=on ajoutée à la liste des
## paramètres du noyau lors du démarrage). Il est préférable d'utiliser
## l'option de configuration slub_debug de la mémoire ajoutée à la liste des
## paramètres du noyau lors du démarrage car elle permet une gestion plus fine
## du slub debug.
#  - name: CONFIG_SLUB_DEBUG
#    value: y
## Nettoie les pages mémoire au moment de leur libération.
#  - name: CONFIG_PAGE_POISONING
#    value: y
## Nettoyage en profondeur désactivé. Cette option a un coût important;
## cependant si l'impact en performance est compatible avec le besoin
## opérationnel de l'équipement il est recommandé de l'activer ( <=5.10)
#  - name: CONFIG_PAGE_POISONING_NO_SANITY
#    value: n
## Le nettoyage des pages mémoire est effectué avec une réécriture de zéros en
## lieu et place des données ( <=5.10)
#  - name: CONFIG_PAGE_POISONING_ZERO
#    value: y
############################### gestion module ##############################
## Activation du support des modules
#  - name: CONFIG_MODULES
#    value: y
## Cette option a remplacé CONFIG_DEBUG_SET_MODULE_RONX dans le noyau ( >=4.11)
#  - name: CONFIG_STRICT_MODULE_RWX
#    value: y
## Les modules doivent être signés. Attention , les modules ne doivent pas être
## strippés après signature.
##  - name: CONFIG_MODULE_SIG
##    value: y
## Empèche le chargement des modules non signés ou signés avec une clé qui ne
## nous appartient pas.
##  - name: CONFIG_MODULE_SIG_FORCE
##    value: y
## Activer CONFIG_MODULE_SIG_ALL permet de signer tous les modules
## automatiquement pendant l'étape "make modules_install", sans cette option
## les modules doivent être signés manuellement en utilisant le script
## scripts/sign -file. L'option CONFIG_MODULE_SIG_ALL dépend de CONFIG_MODULE_SIG
## et CONFIG_MODULE_SIG_FORCE , elles doivent donc être activées.
##  - name: CONFIG_MODULE_SIG_ALL
##    value: y
## La signature des modules utilise SHA512 comme fonction de hash
##  - name: CONFIG_MODULE_SIG_SHA512
##    value: y
##  - name: CONFIG_MODULE_SIG_HASH
##    value: sha512
#
## Indique le chemin vers le fichier contenant à la fois la clé privée et son
## certificat X.509 au format PEM utilisé pour la signature des modules ,
## relatif à la racine du noyau.
##  - name: CONFIG_MODULE_SIG_HASH
##    value: sha512
##  - name: CONFIG_MODULE_SIG_KEY
##    value: "certs/signing_key.pem"
#
#  ############################## évènements anormaux ##############################
#
#  # Émet un rapport sur les conditions d'appel à la macro noyau BUG()
## et tue le processus à l'origine de l'appel. Ne pas positionner cette variable
## peut passer sous silence un certain nombre d'erreurs critiques.
#  - name: CONFIG_BUG
#    value: y
## Arrête le système en cas d'erreur critique pour couper court à tout comportement
## erroné.
#  - name: CONFIG_PANIC_ON_OOPS
#    value: y
## Empêche le redémarrage de la machine suite à un panic ce qui coupe court à
## toute tentative d'attaque par force brute. Ceci a évidemment un impact fort
## sur des serveurs en production.
#  - name: CONFIG_PANIC_TIMEOUT
#    value: -1
#
# ############################## primitives de sécurité ##############################
## Active la possibilité de filtrer les appels système faits par une
## application.
#  - name: CONFIG_SECCOMP
#    value: y
## Active la possibilité d'utiliser des script BPF (Berkeley Packet Filter).
#  - name: CONFIG_SECCOMP_FILTER
#    value: y
## Active les primitives de sécurité du noyau Linux , nécessaire pour les LSM.
#  - name: CONFIG_SECURITY
#    value: y
## Active Yama , qui permet de limiter simplement l'usage de l'appel système
## ptrace (). Une fois les modules de sécurité utilisés par le système
## sélectionné , il convient de désactiver le support des autres modules de
## sécurité dans le noyau afin de réduire la surface d'attaque.
#  - name: CONFIG_SECURITY_YAMA
#    value: y
#
# ############################## plugins du compilateur ##############################
## Active le support des plugins du compilateur (implique l'usage de GCC).
#  - name: CONFIG_GCC_PLUGINS
#    value: y
## Amplifie la génération d'entropie au démarrage de l'équipement pour ceux
## ayant des sources d'entropie inappropriées
#  - name: CONFIG_GCC_PLUGIN_LATENT_ENTROPY
#    value: y
## Nettoie le contenu de la pile au moment de l'appel système exit.
#  - name: CONFIG_GCC_PLUGIN_STACKLEAK
#    value: y
## Impose l'initialisation des structures en mémoire pour éviter la fuite de
## données par superposition avec une ancienne structure.
#  - name: CONFIG_GCC_PLUGIN_STRUCTLEAK
#    value: y
## Globalisation de l'option précédente au cas du passage de structure par
## référence entre fonction si celles -ci ont des champs non initialisés.
#  - name: CONFIG_GCC_PLUGIN_STRUCTLEAK_BYREF_ALL
#    value: y
## Construit un plan mémoire aléatoire pour les structures système du noyau.
## Cette option a un impact fort sur les performances. L'option
## CONFIG_GCC_PLUGIN_RANDSTRUCT_PERFORMANCE =y est à utiliser en substitution si
## cet impact n'est pas acceptable.
#  - name: CONFIG_GCC_PLUGIN_RANDSTRUCT
#    value: y
#
# ############################## pile reseau ##############################
# # Permet de prévenir les attaques de type SYN flood.
#  - name: CONFIG_SYN_COOKIES
#    value: y
# ############################## architectures x86_64 bits ##############################
## Activate le mode complet 64 bits.
#  - name: CONFIG_X86_64
#    value: y
## Interdit l'utilisation d'adresses mémoire en dessous d'une certaine valeur
## (contre -mesure contre les null pointer dereference).
#  - name: CONFIG_DEFAULT_MMAP_MIN_ADDR
#    value: 65536
## Rend non prédictible l'adresse de base du noyau , cette option complexifie
## la tâche d'un attaquant.
#  - name: CONFIG_RANDOMIZE_BASE
#    value: y
## Rend non prédictible l'adresse à laquelle les composants du noyau sont
## exposés dans l'espace d'adressage des processus.
#  - name: CONFIG_RANDOMIZE_MEMORY
#    value: y
## Contre -mesure à l'attaque Meltdown.
#  - name: CONFIG_PAGE_TABLE_ISOLATION
#    value: y
#
#kernel_parameters_not_set:
############################### gestion memoire bp028 ##############################
## Interdit l'accès direct à la mémoire physique.
## En cas de besoin et uniquement dans ce cas , il est possible d'activer un
## accès strict à la mémoire , limitant ainsi son accès , avec les options
## CONFIG_STRICT_DEVMEM =y et CONFIG_IO_STRICT_DEVMEM =y
#  - name: CONFIG_DEVMEM
## Interdit de revenir à une vérification du mapping auprès de l'allocateur si
## l'option précédente a échoué ( <=5.15)
#  - name: CONFIG_HARDENED_USERCOPY_FALLBACK
## Désactive l'usage dangereux de l'ACPI , pouvant entrainer une écriture directe
## en mémoire physique.
#  - name: CONFIG_ACPI_CUSTOM_METHOD
## Interdit tout accès direct à la mémoire du noyau à partir du userspace ( <=5.12)
#  - name: CONFIG_DEVKMEM
## Interdit la fourniture de la disposition de l'image noyau
#  - name: CONFIG_PROC_KCORE
## Désactive la rétrocompatibilité VDSO , qui rend impossible l'usage de l'ASLR
#  - name: CONFIG_COMPAT_VDSO
############################### structures de données bp028 #############################
#
############################### allocateur memoire bp028 ##############################
## Désactive la rétrocompatibilité avec brk() qui rend impossible une
## implémentation de brk() avec l'ASLR.
#  - name: CONFIG_COMPAT_BRK
#
# ############################## primitives de sécurité bp028 ##############################
## Assure que les structures noyau associées aux LSM sont toujours mappées en
## lecture seule après le démarrage du système. Dans le cas où SELinux est
## utilisé , il faut s'assurer que CONFIG_SECURITY_SELINUX_DISABLE is not set ,
## pour que cette option soit disponible. Il n'est alors plus possible de
## désactiver un LSM une fois le noyau démarré. Il est en revanche encore
## possible de le faire en modifiant la ligne de commande du noyau. Pour le
## noyau 4.18 les LSM présents sont: AppArmor , LoadPin , SELinux , Smack , TOMOYO
## et Yama.
#  - name: CONFIG_SECURITY_WRITABLE_HOOKS
#
# ############################## divers comportement ##############################
## Interdit l'exécution d'une nouvelle image noyau à chaud.
#  - name: CONFIG_KEXEC
## Interdit le passage en mode hibernation , qui permet de substituer l'image
## noyau à son insu.
#  - name: CONFIG_HIBERNATION
## Désactive le support de format binaire arbitraire.
#  - name: CONFIG_BINFMT_MISC
## Impose l'utilisation des ptys modernes (devpts) dont on peut contrôler le
## nombre et l'usage.
#  - name: CONFIG_LEGACY_PTYS
## Si le support des modules n'est pas absolument nécessaire celui -ci doit être
## désactivé.
#  - name: CONFIG_MODULES
# ############################## architectures x86_64 bits ##############################
## Désactive la rétro -compatibilité 32 bits , ce qui permet de réduire la
## surface d'attaque mais empêche d'exécuter des binaires 32 bits.
#  - name: CONFIG_IA32_EMULATION
## Interdit la surcharge par processus de la Local Descriptor Table
## (mécanisme lié à l'usage de la segmentation).
#  - name: CONFIG_MODIFY_LDT_SYSCALL
